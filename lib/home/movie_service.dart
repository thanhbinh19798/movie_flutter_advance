import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../environment_config.dart';
import 'movie.dart';
import 'movies_exception.dart';


class MovieService {
  MovieService( this._dio);

  final Dio _dio;

  Future<List<Movie>> getMovies(int page) async {
    await Future.delayed(const Duration(milliseconds: 5000));
    try {
      final response = await _dio.get(
        "https://api.themoviedb.org/3/movie/popular?api_key=d5b97a6fad46348136d87b78895a0c06&language=en-US&page=$page",
      );

      final results = List<Map<String, dynamic>>.from(response.data['results']);

      List<Movie> movies = results
          .map((movieData) => Movie.fromMap(movieData))
          .toList(growable: false);
      return movies;
    } on DioError catch (dioError) {
      throw MoviesException.fromDioError(dioError);
    }
  }
}
