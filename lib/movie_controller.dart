

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'home/movie.dart';
import 'home/movie_service.dart';

class MovieController extends GetxController {

  var movies = <Movie>[].obs;
  late  MovieService movieService;

  static const double _endReachedThreshold = 200;
  static const int _itemsPerPage = 20;

  var _nextPage = 1.obs;
  var _loading = true.obs;
  var canLoadMore = true.obs;
  var isFirstLoad = false.obs;

  final ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    movieService = MovieService(Dio());
    scrollController.addListener(onScrollMovie);
    loadMoreMovie();
  }


  refreshData() async {
    canLoadMore.value = true;
    movies.clear();
    _nextPage.value = 1;
    movies.addAll(await movieService.getMovies(_nextPage.value));
    _nextPage++;

  }

  loadMoreMovie() async {
    _loading.value = true;
    movies.addAll(await movieService.getMovies(_nextPage.value));

    _nextPage++;

    if (movies.length < _itemsPerPage) {
      canLoadMore.value = false;
    }

    _loading.value = false;
  }

  void onScrollMovie() {
    if (!scrollController.hasClients || _loading.value) return;

    final thresholdReached =
        scrollController.position.extentAfter < _endReachedThreshold;

    if (thresholdReached) {
      loadMoreMovie();
    }
  }

}